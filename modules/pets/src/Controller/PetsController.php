<?php

namespace Drupal\pets\Controller;

use Drupal\Core\Controller\ControllerBase;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\pets\Entity\Pets;


/**
 * Returns responses for pets routes.
 */
class PetsController extends ControllerBase {

  /**
   * Callback for POST on `/pets` API method.
   */
  public function create_pet( Request $request ) {
    $params = json_decode( $request->getContent(), TRUE );
    $pet = Pets::create([
      'bundle' => $params['animal'],
      'field_breed' => $params['breed'],
      'field_country' => $params['country'],
      'field_name' => $params['name'],
    ]);
    $pet->save();

    return new JsonResponse( $pet );
  }

  /**
   * Callback for GET on `/pets/dogs` API method.
   */
  public function get_dogs() {
    $node_storage = \Drupal::entityTypeManager()->getStorage('pets');

    $query = \Drupal::entityQuery('pets')
      ->condition('bundle', 'dog');
    $pet_id = $query->execute();
    $pets = $node_storage->loadMultiple($pet_id);
    foreach ($pets as $pet) {
      $response[] = [
        'animal' => $pet->bundle(),
        'name' => $pet->field_name->value,
        'breed' => $pet->field_breed->value,
        'country' => $pet->field_country->value
      ];
    }
    return new JsonResponse( $response );
  }

  /**
   * Callback for GET on `/pets/cats` API method.
   */
  public function get_cats() {
    $node_storage = \Drupal::entityTypeManager()->getStorage('pets');

    $query = \Drupal::entityQuery('pets')
      ->condition('bundle', 'cat');
    $pet_id = $query->execute();
    $pets = $node_storage->loadMultiple($pet_id);

    foreach ($pets as $pet) {
      $response[] = [
        'animal' => $pet->bundle(),
        'name' => $pet->field_name->value,
        'breed' => $pet->field_breed->value,
        'country' => $pet->field_country->value
      ];
    }
    return new JsonResponse( $response );
  }
}
