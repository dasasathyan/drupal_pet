<?php

namespace Drupal\pets\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\pets\PetsInterface;

/**
 * Defines the pets entity class.
 *
 * @ContentEntityType(
 *   id = "pets",
 *   label = @Translation("pets"),
 *   label_collection = @Translation("petses"),
 *   bundle_label = @Translation("pets type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\pets\PetsListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\pets\PetsAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\pets\Form\PetsForm",
 *       "edit" = "Drupal\pets\Form\PetsForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "pets",
 *   admin_permission = "administer pets types",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/pets/add/{pets_type}",
 *     "add-page" = "/admin/content/pets/add",
 *     "canonical" = "/pets/{pets}",
 *     "edit-form" = "/admin/content/pets/{pets}/edit",
 *     "delete-form" = "/admin/content/pets/{pets}/delete",
 *     "collection" = "/admin/content/pets"
 *   },
 *   bundle_entity_type = "pets_type",
 *   field_ui_base_route = "entity.pets_type.edit_form"
 * )
 */
class Pets extends ContentEntityBase implements PetsInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the pets was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the pets was last edited.'));

    return $fields;
  }

}
