<?php

namespace Drupal\pets\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the pets type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "pets_type",
 *   label = @Translation("pets type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\pets\Form\PetsTypeForm",
 *       "edit" = "Drupal\pets\Form\PetsTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\pets\PetsTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer pets types",
 *   bundle_of = "pets",
 *   config_prefix = "pets_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/pets_types/add",
 *     "edit-form" = "/admin/structure/pets_types/manage/{pets_type}",
 *     "delete-form" = "/admin/structure/pets_types/manage/{pets_type}/delete",
 *     "collection" = "/admin/structure/pets_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class PetsType extends ConfigEntityBundleBase {

  /**
   * The machine name of this pets type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the pets type.
   *
   * @var string
   */
  protected $label;

}
